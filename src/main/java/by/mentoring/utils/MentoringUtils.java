package by.mentoring.utils;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

public final class MentoringUtils {
    static {
        BasicConfigurator.configure();
    }
    private  final static Logger log = Logger.getLogger(MentoringUtils.class);

    public static void printTaskHeader(String name) {
        log.info("||==========|| " + name);
    }

    public static void printTaskDelimeter() {
        log.info("||==========");
    }

    public static void printTaskFooter() {
        log.info("||==========||");
    }

    public static void println(final String string) {
        final String[] strings = string.split("\n");
        for (final String str : strings){
            log.info("|| " + str);
        }
    }

    public static Logger getLogger(){
        return log;
    }
}